//
// Created by tumpek on 2022. 03. 24..
//

#ifndef ONLAB_MYARRAY_HPP
#define ONLAB_MYARRAY_HPP

#include "MyFilter.h"

// It implements an object-oriented Filter-container, with its max size given as template parameter
// The pattern may not be the best, though it could be more memory-friendly (but it results in an extra indirection)
// It may be corrected in the future by changing this class's implementation
// The pattern was given, I only made it encapsulated and predictable
template<int MAX_SIZE>
class MyArray {
    MyFilter* array[MAX_SIZE];
    int size = 0;

    MyArray(MyArray& rhs) {}
    MyArray& operator=(MyArray& rhs) {}
public:

    MyArray() {}
    int getSize() const { return size; }
    MyFilter* operator[](int idx) { return array[idx]; }

    // It is anticipated that the parameter points on a manually reserved heap address,
    // and it will not be freed later by the caller
    void addFilter(MyFilter* filter){

        // This check may be redundant since I will explicitly check this before invoking it
        // in my code, but the function is not used in loops, so I'll leave it in to make sure
        if(size >= MAX_SIZE) {
            delete filter;
            return;
        }
        array[size] = filter;
        size++;
    }

    // Not necessary, but it's talkative code
    bool isFull() const { return size == MAX_SIZE; }

    ~MyArray(){
        for(int i = 0; i < size; i++){
            delete array[i];
        }
    }
};

#endif //ONLAB_MYARRAY_HPP
