//
// Created by tumpek on 2022. 03. 07..
//

#include "MyFilter.h"

MyFilter::MyFilter(unsigned int sampleRate) : sampleRate(sampleRate),
                                                                        b0(0), b1(0), b2(0), a1(0), a2(0) {}

// Ez a függvény fel van készítve arra, hogy az input és output is ugyanaz, illetve arra is,
// hogy többször hívják ugyanahhoz a hangfájlhoz
void MyFilter::processMono(float* input, float* output, unsigned int numberOfSamples) {

    for (unsigned int i = 0; i < numberOfSamples; i++) {
        double temp_input = input[i];

        output[i] = input_i_m2 * b2 + input_i_m1 * b1 + input[i] * b0 - output_i_m2 * a2 - output_i_m1 * a1;
        
        // Feedback állapotváltozók léptetése
        output_i_m2 = output_i_m1;
        output_i_m1 = output[i];
        
        input_i_m2 = input_i_m1;
        input_i_m1 = temp_input;
    }
}

void MyFilter::setParametricParameters(float frequency, float octaveWidth,
                                     float dbGain) {

    // Created based on the matlab sketch I was given by Márki Ferenc

    // Frequency wrapping
    frequency = sampleRate * 2.0f * tanf(frequency * PI_2 / (2 * sampleRate)) / (PI_2);

    float omega = PI_2 * frequency;
    float omega0 = atanf(omega / 2.0 / sampleRate) * 2.0;
    float gamma = sinhf(logf(2) / 2 * octaveWidth * omega0 / sinf(omega0)) * sinf(omega0);
    float K = powf(10, dbGain / 20);
    float sqrtK = sqrtf(K);

    // Az osztások második tagja mindig ugyanaz, a biztonság kedvéért optimalizálom a kódban
    // Ha valakinek nem tetszik, kibonthatja, ugyanins ettől a kód potenciálisan nehezebben látható át
    float oszto = (1 + gamma / sqrtK);

    b0 = (1 + gamma * sqrtK) / oszto;
    b1 = (-2 * cosf(omega0)) / oszto;
    b2 = (1 - gamma * sqrtK) / oszto;
    // a0 = 1.0f;
    a1 = b1; // (-2 * cos(omega0)) / (1 + gamma / sqrt(K));
    a2 = (1 - gamma / sqrtK) / oszto;
}


void MyFilter::setLowPassParameters(float cutOffFreqency) {

    float ita = 1.0f / tanf(M_PI * cutOffFreqency / sampleRate);
    float ita2 = ita * ita;

    b0 = 1.0f / (1.0f + q * ita + ita2);
    b1 = 2.0f * b0;
    b2 = b0;
    a1 = -2.0f * (ita2 - 1) * b0;
    a2 = (1.0f - q * ita + ita2) * b0;
}

void MyFilter::setHighPassParameters(float cutOffFrequency) {
    float wca = tanf(M_PI * cutOffFrequency / sampleRate); // by Márki: equivalent analogue filter cut-off
    float wca2 = wca * wca;
    float K = wca2 + q * wca + 1.0f;
    
    b0 = 1.0f/K;
    b1 = -2.0f/K;
    b2 = b0;

    a1 = (2.0f * wca2 - 2.0f) / K;
    a2 = (wca2 - q * wca + 1.0f) / K;
}

void MyFilter::setResonantHighPassParameters(float cutOff, float resonance) {
    setHighPassParameters(cutOff);
}

void MyFilter::setResonantLowPassParameters(float cutOff, float resonance) {
    setLowPassParameters(cutOff);
}