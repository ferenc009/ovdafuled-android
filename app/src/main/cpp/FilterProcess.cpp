//
// Created by arons on 2018. 02. 28..
//
#include "FilterProcess.h"

FilterProcess::FilterProcess(unsigned int samplerate) : samplerate(samplerate) {
}

// This destructor wont be necessary, since I encapsulated its logic into the container


void FilterProcess::filterProcess(MyArray<MAX_FILTER_NUM>& filterList, float* array, unsigned int numberOfSamples) {
    for (int i = 0; i < filterList.getSize(); i++) {
        filterList[i]->processMono(array, array, numberOfSamples);
    }
}

void FilterProcess::filterProcessA(float *array, unsigned int numberOfSamples) {
    filterProcess(aFilterList, array, numberOfSamples);
}

void FilterProcess::filterProcessC(float *array, unsigned int numberOfSamples) {
    filterProcess(cFilterList, array, numberOfSamples);
}

static FilterProcess *fp = NULL;

extern "C" JNIEXPORT void
Java_hu_bme_hit_last_ovdafuled_FilterPlugin_filterProcessDelete(
        JNIEnv *javaEnvironment,
        jobject __unused obj) {
    delete fp;
}

extern "C" JNIEXPORT void
Java_hu_bme_hit_last_ovdafuled_FilterPlugin_filterProcessCreate(
        JNIEnv *javaEnvironment,
        jobject __unused obj, jint samplerate) {
    fp = new FilterProcess((unsigned int) samplerate);
}

// These functions are code duplications, but because they are part of the NDK's API, I wont change them
extern "C" JNIEXPORT void
Java_hu_bme_hit_last_ovdafuled_FilterPlugin_filterProcessingA(JNIEnv *javaEnvironment,
                                                              jclass obj,
                                                                            jfloatArray input,
                                                                            jfloatArray output,
                                                                            jint numberOfSamples) {

    // Reserve memory for the elements and copy the elements in the java array into a C array
    jfloat *inputArr = javaEnvironment->GetFloatArrayElements(input, JNI_FALSE);

    // Filter the samples in-place within the C array
    fp->filterProcessA(inputArr, (unsigned int) numberOfSamples);

    // Copy the C array back into the java array
    javaEnvironment->SetFloatArrayRegion(output, 0, numberOfSamples, inputArr);

    // Free the reserved memory
    javaEnvironment->ReleaseFloatArrayElements(input, inputArr, JNI_ABORT);

    /* The ReleaseFloatArrayElements function does not copy the array back, only frees only,
     * this behaviour is specified in the mode parameter
     * Possible modes.
     * 0 -> Copies back the array and frees the memory
     * JNI_COMMIT -> Copies back the array, but does not free the memory
     * JNI_ABORT -> Does not copy back the array, but frees the memory <- This mode is used here
     */
}

// These functions are code duplications, but because they are part of the NDK's API, I wont change them
extern "C" JNIEXPORT void
Java_hu_bme_hit_last_ovdafuled_FilterPlugin_filterProcessingC(JNIEnv *javaEnvironment,
                                                                            jobject __unused obj,
                                                                            jfloatArray input,
                                                                            jfloatArray output,
                                                                            jint numberOfSamples) {

    // Reserve memory for the elements and copy the elements in the java array into a C array
    jfloat *inputArr = javaEnvironment->GetFloatArrayElements(input, JNI_FALSE);

    // Filter the samples in-place within the C array
    fp->filterProcessC(inputArr, (unsigned int) numberOfSamples);

    // Copy the C array back into the java array
    javaEnvironment->SetFloatArrayRegion(output, 0, numberOfSamples, inputArr);

    // Free the reserved memory
    javaEnvironment->ReleaseFloatArrayElements(input, inputArr, JNI_ABORT);

    /* The ReleaseFloatArrayElements function does not copy the array back, only frees the memory,
     * this behaviour is specified in the mode parameter
     * Possible modes:
     * 0 -> Copies back the array and frees the memory
     * JNI_COMMIT -> Copies back the array, but does not free the memory
     * JNI_ABORT -> Does not copy back the array, but frees the memory <- This mode is used here
     */
}

extern "C" JNIEXPORT jint
Java_hu_bme_hit_last_ovdafuled_FilterPlugin_addParametricFilterA(
        JNIEnv *javaEnvironment,
        jobject __unused obj,
        jfloat frequency,
        jfloat octaveWidth,
        jfloat dbGain) {


    if (fp->aFilterList.isFull())
        return fp->aFilterList.getSize();

    MyFilter *paramFilter = new MyFilter(fp->samplerate);
    paramFilter->setParametricParameters(frequency, octaveWidth, dbGain);

    fp->aFilterList.addFilter(paramFilter);

    return fp->aFilterList.getSize();
}

enum FILTER_TYPES {
    HPF, LPF, Parametric
};

extern "C" JNIEXPORT jint
Java_hu_bme_hit_last_ovdafuled_FilterPlugin_addResonantFilterA(
        JNIEnv *javaEnvironment,
        jobject __unused obj,
        jint filterType,      // see enum FILTER_TYPES
        jfloat cutOffFreqency,   // cut-off frequency
        jfloat resonance) { // resonance at cut-off frequency


    if (fp->aFilterList.isFull()) {
        return fp->aFilterList.getSize();
    }

    MyFilter *res_filt = new MyFilter(fp->samplerate);

    switch (filterType) {
        case HPF:
            res_filt->setResonantHighPassParameters(cutOffFreqency, resonance);
            break;
        case LPF:
            res_filt->setResonantLowPassParameters(cutOffFreqency, resonance);
            break;
        default :
            return 0;
    }

    fp->aFilterList.addFilter(res_filt);

    return fp->aFilterList.getSize();
}

extern "C" JNIEXPORT jint
Java_hu_bme_hit_last_ovdafuled_FilterPlugin_addParametricFilterC(
        JNIEnv *javaEnvironment,
        jobject __unused obj,
        jfloat frequency,
        jfloat octaveWidth,
        jfloat dbGain) {
    if (fp->cFilterList.isFull()) {
        return fp->cFilterList.getSize();
    }
    MyFilter *paramFilter = new MyFilter(fp->samplerate);
    paramFilter->setParametricParameters(frequency, octaveWidth, dbGain);

    fp->cFilterList.addFilter(paramFilter);

    return fp->cFilterList.getSize();
}

extern "C" JNIEXPORT jint
Java_hu_bme_hit_last_ovdafuled_FilterPlugin_addResonantFilterC(
        JNIEnv *javaEnvironment,
        jobject __unused obj,
        jint filterType,      // see enum FILTER_TYPES
        jfloat cutOffFreqency,   // cut-off frequency
        jfloat resonance) { // resonance at cut-off frequency


    if (fp->cFilterList.isFull()) {
        return fp->cFilterList.getSize();
    }

    MyFilter *res_filt = new MyFilter(fp->samplerate);

    switch (filterType) {
        case HPF:
            res_filt->setResonantHighPassParameters(cutOffFreqency, resonance);
            break;
        case LPF:
            res_filt->setResonantLowPassParameters(cutOffFreqency, resonance);
            break;
        default:
            return 0;
    }

    fp->cFilterList.addFilter(res_filt);

    return fp->cFilterList.getSize();
}

