//
// Created by arons on 2018. 02. 28..
//

#ifndef FILTERPROCESS_H
#define FILTERPROCESS_H

// It also includes MyFilterBase.h
#include "MyArray.hpp"
#include <jni.h>


class FilterProcess {
public:
    static const int MAX_FILTER_NUM = 30;
private:
    // Created by Tumpek Fecó to avoid code duplication
    // Calls the filtering method on the given filterList
    void filterProcess(MyArray<MAX_FILTER_NUM>& filterList, float* array, unsigned int numberOfSamples);

public:
    MyArray<MAX_FILTER_NUM> aFilterList, cFilterList;
    unsigned int samplerate;
    FilterProcess *fp;


    FilterProcess(unsigned int samplerate);

    void filterProcessA(float *array, unsigned int numberOfSamples);

    void filterProcessC(float *array, unsigned int numberOfSamples);


};


#endif //FILTERPROCESS_H
