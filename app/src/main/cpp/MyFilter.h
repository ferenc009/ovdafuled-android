//
// Created by tumpek on 2022. 03. 07..
//

#ifndef ONLAB_MYFILTER_H
#define ONLAB_MYFILTER_H

#include <cmath>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif // !M_PI

class MyFilter {

    // Filter feedback states
    float input_i_m1 = 0;
    float input_i_m2 = 0;
    float output_i_m1 = 0;
    float output_i_m2 = 0;

protected:
    unsigned int sampleRate;
    static constexpr float PI_2 = (M_PI*2.0);
    static constexpr float q = 1.414213562373f;
    // Parameters
    float b0;
    float b1;
    float b2;
    // float a0; // <-- deleted, it as always 1
    float a1;
    float a2;

    void setLowPassParameters(float cutOff);
    void setHighPassParameters(float cutOff);

public:
    MyFilter(unsigned int sampleRate);
    void processMono(float* input, float* output, unsigned int numberOfSamples);
    void setParametricParameters(float frequency, float octaveWidth, float dbGain);

    void setResonantHighPassParameters(float cutOff, float resonance);
    void setResonantLowPassParameters(float cutOff, float resonance);
};

#endif //ONLAB_MYFILTER_H
