package hu.bme.hit.last.ovdafuled;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceFragmentCompat;

import hu.bme.hit.last.ovdafuled.services.RecorderService;


/**
 * Created by Aron Fabian on 2018. 04. 16..
 */
public class SettingsFragment extends PreferenceFragmentCompat {
    private SharedPreferences prefs;
    SharedPreferences.OnSharedPreferenceChangeListener listener;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.app_preferences, rootKey);
        prefs = getPreferenceManager().getSharedPreferences();
        listener =
                new SharedPreferences.OnSharedPreferenceChangeListener() {
                    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                        switch (key) {
                            case "class_type":
                                RecorderService.classType = prefs.getString("class_type", Constants.MEASUREMENT_CLASS.CLASS_ONE);
                                break;
                            case "rms_time":
                                String rmsTime = prefs.getString("rms_time", "sec");
                                RecorderService.setRmsUpdateTime(rmsTime);
                                break;
//                            case "calibration":
//                                RecorderService.saveFile = prefs.getBoolean("calibration", false);
//                                break;
                            default:
                                break;
                        }
                    }
                };

        prefs.registerOnSharedPreferenceChangeListener(listener);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        Toolbar toolbar = root.findViewById(R.id.generic_toolbar);
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return root;
    }

    @Override
    public void onPause() {
        super.onPause();
        prefs.unregisterOnSharedPreferenceChangeListener(listener);
    }

    @Override
    public void onResume() {
        super.onResume();
        prefs.registerOnSharedPreferenceChangeListener(listener);
    }
}
