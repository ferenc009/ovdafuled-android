package hu.bme.hit.last.ovdafuled.drive

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import hu.bme.hit.last.ovdafuled.R

class MyListAdapter(private val list: List<MyDrive.FDescription>) : BaseAdapter(){
    override fun getCount() = list.size

    override fun getItem(p0: Int) = list[p0]

    override fun getItemId(p0: Int) = list[p0].hashCode().toLong()

    override fun getView(position: Int, convertView: View?, container: ViewGroup?): View {
        val newView = convertView?: LayoutInflater.from(container?.context).inflate(R.layout.device_entry, container, false)
        (newView as TextView).text = getItem(position).name
        return newView
    }
}