package hu.bme.hit.last.ovdafuled.drive

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import com.google.api.client.http.FileContent
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import com.google.api.services.drive.model.File
import com.google.api.services.drive.model.FileList
import hu.bme.hit.last.ovdafuled.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.lang.Exception
import java.util.concurrent.CancellationException


class MyDrive(private val context: Context) {
    companion object {
        private const val GTAG = "FECO"
        private const val API_KEY =
            "AIzaSyANqvfYsGqb0wdCWCqigQusJcuuXagFl4w"
    }

    private lateinit var drive: Drive
    private lateinit var folderID: String
    private lateinit var initJob: Job
    private val secondInitJob = Job()
    var listFileJob: Job? = null
    private var permissionsNeeded: Boolean = false

    fun initDrive(googleAccount: GoogleSignInAccount, owner: LifecycleOwner, launcher: ActivityResultLauncher<Intent>){
        val credential = GoogleAccountCredential.usingOAuth2(context, listOf(DriveScopes.DRIVE_FILE))
        credential.selectedAccount = googleAccount.account
        drive = Drive.Builder(
            NetHttpTransport(),
            GsonFactory(),
            credential,
        ).setApplicationName(context.getString(R.string.app_name))
            .build()

        initJob = owner.lifecycleScope.launch(Dispatchers.IO) {

            val result: FileList?
            // Try if it succeeds
            try {
                result = queryFolder()
            } catch (e: UserRecoverableAuthIOException){
                withContext(Dispatchers.Main) {
                    // This is kinda mindfuck, but if I change the value
                    // before I launch it should be formally unbreakable
                    permissionsNeeded = true
                    launcher.launch(e.intent)
                }
                return@launch
            }


            if(result == null)
                return@launch

            findFolder(result)
        }
    }

    // This function should only be called if the driveInit function fails with
    // exception and the problem has been resolved
    fun initAfterPermission(owner: LifecycleOwner){
        owner.lifecycleScope.launch(secondInitJob + Dispatchers.IO){
            findFolder(queryFolder())
            secondInitJob.complete()
        }
    }

    // Contains blocking code, don't user on main thread
    private fun findFolder(result: FileList){

        // If it doesn't exist, we create it and store its ID
        if (result.files.size == 0) {
            val folder = com.google.api.services.drive.model.File().apply {
                name = context.getString(R.string.drive_folder_name)
                mimeType = "application/vnd.google-apps.folder"
            }
            folderID = drive.files().create(folder)
                .setFields("id")
                .execute()
                .id
            Log.d(GTAG, "Folder didn't exist, we created it")
        } else {
            // If folders exist we use the first one to store the data
            folderID = result.files[0].id
            Log.d(GTAG, "folder existed its name: ${result.files[0].name}")
        }
    }

    // Blocking code present, don't use on main thread
    private fun queryFolder() = drive.files().list().apply {
        q = "mimeType='application/vnd.google-apps.folder'" // Querying the folder types we created
        spaces = "drive" // This is the only space we are eligible to access
        fields = "files(id, name)"
        pageToken = null
    }.execute()

    fun downloadFile(owner: LifecycleOwner, fDescript: FDescription, callback: (String?) -> Unit){
        owner.lifecycleScope.launch(Dispatchers.IO){
            val out = ByteArrayOutputStream()
            try {
                drive.files().get(fDescript.id)
                    .executeMediaAndDownloadTo(out)
            } catch (e: Exception){
                withContext(Dispatchers.Main) {
                    callback(null)
                }
            }
            withContext(Dispatchers.Main){
                callback(out.toString())
            }
            Log.d(GTAG, out.toString())
        }
    }


    fun uploadFile(owner: LifecycleOwner, file: java.io.File, callback: (String) -> Unit) {
        val gFile = File().apply {
            name = file.name
            parents = listOf(folderID)
        }

        val content = FileContent("text/plain", file)

        owner.lifecycleScope.launch(Dispatchers.IO) {
            try {
                drive.files().create(gFile, content)
                    .setFields("id, parents")
                    .execute()
            } catch (e: Exception){
                withContext(Dispatchers.Main){
                    callback(context.getString(R.string.drive_upload_failed))
                }
            }
            withContext(Dispatchers.Main) {
                callback(context.getString(R.string.drive_upload_successful))
            }
        }
    }

    data class FDescription(
       val name: String,
       val id: String,
    )

    fun listFiles(owner: LifecycleOwner, callback: (java.util.ArrayList<FDescription>) -> Unit) {
        owner.lifecycleScope.launch(Dispatchers.IO) {

            // We need to have the init process completed before proceeding any further
            initJob.join()
            try {
                // If we don't have permission it freezes there
                if (permissionsNeeded) {
                    Log.d(GTAG, "permission was needed, secondjob join")
                    secondInitJob.join()
                }
            } catch (e: CancellationException){
                Log.d(GTAG, "Listfiles cancelled. Cause: ${e.message}")
                return@launch
            }

            val fileList = java.util.ArrayList<FDescription>()
            var pToken: String? = null
            do {
                val result = drive.files().list().apply {
                    q =
                        "mimeType!='application/vnd.google-apps.folder'" // Querying the non-folder types we created
                    spaces = "drive" // This is the only space we are eligible to access
                    fields = "files(id, name)"
                    pageToken = pToken
                }.execute()

                result.files.forEach { file ->
                    fileList.add(FDescription(file.name.substring(0, file.name.length-4), file.id))
                }

                pToken = result.nextPageToken
            } while (pToken != null)

            withContext(Dispatchers.Main) {
                // The list is ready, we give it back
                callback(fileList)
            }
        }
    }


}