package hu.bme.hit.last.ovdafuled.activities;

import android.Manifest;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CancellationException;

import hu.bme.hit.last.ovdafuled.CalibrationType;
import hu.bme.hit.last.ovdafuled.Constants;
import hu.bme.hit.last.ovdafuled.DeviceName;
import hu.bme.hit.last.ovdafuled.R;
import hu.bme.hit.last.ovdafuled.drive.DeviceDialogFragment;
import hu.bme.hit.last.ovdafuled.drive.MyDrive;
import hu.bme.hit.last.ovdafuled.services.RecorderService;
import hu.bme.hit.last.ovdafuled.Time;
import kotlin.Unit;
import kotlinx.coroutines.Job;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class MainActivity extends AppCompatActivity {


    private final MainActivity target = this;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private int requestCount = 0; // Megértési célokra, végső apk-ból kivenni

    ConnectivityManager cm;
    private GoogleSignInAccount account;
    private GoogleSignInClient client;
    private MyDrive drive;
    ActivityResultLauncher<Intent> drivePermission;
    SharedPreferences prefs;

    private TextView calibText;
    private View startStopButton;
    private TextView dBAText;
    private TextView lAeqText;
    private BoxedText u3Text;
    private BoxedText b314Text;
    private BoxedText b1418Text;
    private TextView overloadText;
    private View overloadBox;
    private TextView startStopText;
    private ImageButton recordButton;
    private int measLengthSec;
    public int overloadSumCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startStopButton = findViewById(R.id.record_image);
        toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        calibText = findViewById(R.id.text_calib);
        startStopText = findViewById(R.id.start_stop_text);
        progressBar = findViewById(R.id.progressBar);
        dBAText = findViewById(R.id.text_dba);
        // dBCText = findViewById(R.id.text_dbc);
        lAeqText = findViewById(R.id.value_laeq);
        u3Text = new BoxedText(findViewById(R.id.text_u3_recom), findViewById(R.id.layout_under_3), this);
        b314Text = new BoxedText(findViewById(R.id.text_3_14_recom), findViewById(R.id.layout_3_14), this);
        b1418Text = new BoxedText(findViewById(R.id.text_14_18_recom), findViewById(R.id.layout_14_18), this);
        // overloadText = findViewById(R.id.text_overdrive);
        overloadBox = findViewById(R.id.box_overdrive);
        overloadBox.setVisibility(View.INVISIBLE);

        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        if (RecorderService.isAlive)
            makeStopButton();
        else makeStartButton();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }

        if (isReasonToDrive()) {
            drivePermission = registerForActivityResult(
                    new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if(result.getResultCode() == RESULT_OK){
                           drive.initAfterPermission(this);
                        }
                        else {
                            dialogError(R.string.drive_declined);
                            // prefs.edit().putString(Constants.DRIVE_STATUS, "declined").apply();
                            Job listJob = drive.getListFileJob();
                            if(listJob != null)
                                drive.getListFileJob().cancel(new CancellationException("No permission given by the user"));
                        }
                    }
            ); // We need to register it so we can know if the user has given us permission
            drive = new MyDrive(getApplicationContext());
            signIn();
            getPhoneInfo();
        }
    }

    private boolean isReasonToDrive() {
        if (!isOnline())
            return false;
        String status = prefs.getString(Constants.DRIVE_STATUS_KEY, null);
        if (status == null)
            return true;
        if (status.equals("declined"))
            return false;
        if (status.equals("done"))
            return false;
        return true;
    }

    private void driveInit() {
        drive.initDrive(account, this, drivePermission);
        drive.listFiles(this, this::showDriveFiles);
    }

    private Unit showDriveFiles(ArrayList<MyDrive.FDescription> list){
        DeviceDialogFragment fragment = new DeviceDialogFragment(
                list,
                (position) -> {
                    getDeviceIdentifierFromDrive(list.get(position));
                    return Unit.INSTANCE;
                },
                this::createDeviceFile
        );
        fragment.show(getSupportFragmentManager(), "DeviceFragment");
        return Unit.INSTANCE;
    }

    private Unit createDeviceFile(String name){
        String id = getAndroidID();

        String path = getExternalFilesDir(null).getPath() + "/" + name + ".txt";
        File file = new File(path);
        try {
            FileWriter fw = new FileWriter(file);
            fw.write(id);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("FECO", "A fájlba nem tudtam írni");
        }
        Log.d("FECO", file.exists()? "Létezik" : "Nem létezik");

        drive.uploadFile(this, file, (message) -> {
            toastError(message);
            prefs.edit()
                    .putString(Constants.UNIQUE_IDENTIFIER_KEY, Constants.deviceUniqueID)
                    .putString(Constants.DRIVE_STATUS_KEY, "done")
                    .apply();
            return Unit.INSTANCE;
        });

        Log.d("FECO", "Készen: új név: " + name);
        return Unit.INSTANCE;
    }

    private void getDeviceIdentifierFromDrive(MyDrive.FDescription file){
        drive.downloadFile(this, file, (result) -> {
            if(result == null){
                toastError(R.string.drive_download_failed);
                return Unit.INSTANCE;
            }
            toastError(R.string.drive_download_successful);
            prefs.edit()
                    .putString(Constants.UNIQUE_IDENTIFIER_KEY, result)
                    .putString(Constants.DRIVE_STATUS_KEY, "done")
                    .apply();
            return Unit.INSTANCE;
        });
    }

    private void signIn() {
        GoogleSignInAccount temp = GoogleSignIn.getLastSignedInAccount(this);
        if (temp != null) {
            account = temp;
            driveInit();
            return;
        }
        client = GoogleSignIn.getClient(this,
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(Constants.WEB_CREDENTIAL_ID)
                        .requestEmail()
                        .build()
        );
        ActivityResultLauncher<Intent> launcher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                (ActivityResult result) -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // According to the documentation, there  is no need to subscribe to the task's completion
                        try {
                            account = GoogleSignIn.getSignedInAccountFromIntent(result.getData()).getResult(ApiException.class);
                            Log.d("FECO", account.toString());
                            driveInit();
                        } catch (ApiException e) {
                            dialogError(R.string.sign_in_failed);
                            //prefs.edit().putString(Constants.DRIVE_STATUS, "declined").apply();
                        }
                    } else {
                        dialogError(R.string.sign_in_failed);
                        //prefs.edit().putString(Constants.DRIVE_STATUS, "declined").apply();
                    }
                }
        );
        launcher.launch(client.getSignInIntent());
    }

    static class BoxedText {
        public final TextView textView;
        public final View box;
        private final int horizontal;
        private final int vertical;
        private final MainActivity ctx;

        public BoxedText(TextView t, View b, MainActivity ctx) {
            textView = t;
            box = b;
            this.ctx = ctx;
            horizontal = ctx.getResources().getDimensionPixelSize(R.dimen.recom_box_horizontal_padding);
            vertical = ctx.getResources().getDimensionPixelSize(R.dimen.recom_box_vertical_padding);
        }

        private void setBoxBackground(@DrawableRes int drawable) {
            box.setBackground(ContextCompat.getDrawable(ctx, drawable));
            box.setPadding(horizontal, vertical, horizontal, vertical);
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        // The id of the channel.
        String id = Constants.CHANNEL_ID;
        // The user-visible name of the channel.
        CharSequence name = "Media playback";
        // The user-visible description of the channel.
        String description = "Media playback controls";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
        // Configure the notification channel.
        mChannel.setDescription(description);
        mChannel.setShowBadge(false);
        mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        mNotificationManager.createNotificationChannel(mChannel);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private String getAndroidID() {
        return Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public void getPhoneInfo() {
        requestCount++; // Debug célokra
        Log.d("FECO", String.format("Info requests: %d", requestCount)); // Debug célokra
        DeviceName.with(this).request(new DeviceName.Callback() {
            @Override
            public void onFinished(DeviceName.DeviceInfo info, Exception error) {
                Constants.deviceModel = info.model;
                if (info.marketName.equals(info.model)) {
                    Constants.deviceMarketName = null;
                } else {
                    Constants.deviceMarketName = info.manufacturer + " " + info.marketName;
                }

                Constants.deviceUniqueID = prefs.getString(Constants.UNIQUE_IDENTIFIER_KEY, getAndroidID());

                if ((info.marketName.equals(Build.MODEL)) && !isOnline()) {
                    moreDeviceInfo();
                }

            }
        });
    }

    private void moreDeviceInfo() {
        new AlertDialog.Builder(this)
                .setCancelable(true)
                .setMessage(R.string.more_device_info)
                .show();
    }

    // Ez jó
    public boolean isOnline() {
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    List<String> LAHistory = new ArrayList<>();
    List<String> LCHistory = new ArrayList<>();
    double lAeq = 0;
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (Objects.requireNonNull(intent.getAction())) {
                case Constants.ACTION.DBA_DBC_BROADCAST_ACTION:
                    double dBA = intent.getDoubleExtra("dBA", 0);
                    double dBC = intent.getDoubleExtra("dBC_max", 0);
                    //dBCText.setText((int) dBC + "dBCmax");
                    // dBCText.setText(String.format("%.1f dBCmax",dBC));
                    LAHistory.add(String.format("%.1f", dBA));
                    LCHistory.add(String.format("%.1f", dBC));
                    // progressbar value checks
                    if (dBA < 0) {
                        dBA = 0;
                    }
                    if (dBA > progressBar.getMax()) {
                        dBA = progressBar.getMax();
                    }
                    ObjectAnimator.ofInt(progressBar, "progress", (int) dBA).start();
                    //dBAText.setText(String.valueOf((int) dBA) + "dBA");
                    dBAText.setText(String.format("%.1f dBA", dBA));
                    Log.d("MAIN", String.valueOf(dBA));
                    break;
                case Constants.ACTION.LAEQ_BROADCAST_ACTION:
                    measLengthSec = intent.getIntExtra("measLength", 0);
                    lAeq = intent.getDoubleExtra("LAeq", 0);
                    if (measLengthSec > 60) {
                        lAeqText.setText(String.format("%.1f dB", lAeq));
                    }
                    setRecommendationTexts(lAeq);
                    int min = measLengthSec / 60;
                    int sec = measLengthSec - (min * 60);
                    int h = min / 60;
                    min = min - (h * 60);
                    // timeText.setText(String.format("%02d:%02d:%02d",h,min,sec));
                    overloadSumCount += intent.getIntExtra("overloadCount", 0);
                    Log.d("tulvezminta", String.valueOf(overloadSumCount));
                    overloadCategory();
                    break;
                case ConnectivityManager.CONNECTIVITY_ACTION:
                    if (isOnline()) {
                        getPhoneInfo();
                    }
                    break;
                case Constants.ACTION.RECORDERSTOPPED_ACTION:
                    makeStartButton();
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                    SharedPreferences.Editor prefEditor = prefs.edit();
                    prefEditor.putString(Constants.LA_HISTORY, LAHistory.toString());
                    prefEditor.putString(Constants.LC_HISTORY, LCHistory.toString());
                    makeCSV();
//                    int spl_rms = 0;
//                    if (LAHistory.size() > 0){
//                        float f = Float.valueOf(LAHistory.get(LAHistory.size()-1).replace(",","."));
//                        if (Float.isNaN(f) || Float.isInfinite(f)){
//                            spl_rms = 0;
//                        } else {
//                            spl_rms = Math.round(f);
//                        }
//                    }
//                    Log.d("spl_rms",String.valueOf(spl_rms));
                    prefEditor.putString(Constants.LAEQ_LAST, String.valueOf(Math.round(lAeq)));
                    prefEditor.apply();
                    Intent formIntent = new Intent(context, FormActivity.class);
                    startActivity(formIntent);
                    break;
                default:
                    break;
            }

        }
    };

    private void makeCSV() {
        Thread fileWriterThread = new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                String rmsTime = prefs.getString("rms_time", "sec");
                String startTime = prefs.getString(Constants.START_TIME, "0");
                File dir = new File(getExternalFilesDir(null).getPath() + "/Zajszintmero/");
                dir.mkdirs();
                File file = new File(dir, startTime + "_measurement.csv");
                CSVWriter writer = null;

                int incValue = 1000;
                if (rmsTime.equals("sec")) {
                    incValue = 1000;
                } else if (rmsTime.equals("milli")) {
                    incValue = 100;
                }
                try {
                    writer = new CSVWriter(new FileWriter(file));
                    List<String[]> data = new ArrayList<>();
                    Time time = new Time(0);
                    data.add(new String[]{"Time", "LAeq", "LCmax"});
                    for (int i = 0; i < LAHistory.size(); i++) {
                        data.add(new String[]{time.getTimeString(), LAHistory.get(i), LCHistory.get(i)});
                        time.increment(incValue);
                    }
                    writer.writeAll(data);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        writer.close();
                        LAHistory.clear();
                        LCHistory.clear();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }
        });
        fileWriterThread.start();

    }

    private void overloadCategory() {
        if (overloadSumCount > 0) {
            overloadBox.setVisibility(View.VISIBLE);
            /*if (overloadSumCount > measLengthSec*4410){
                overloadText.setTextColor(Color.RED);
            }else{
                overloadText.setTextColor(Color.rgb(255,140,0));
            }*/
        } else {
            overloadBox.setVisibility(View.INVISIBLE);
        }
    }

    private void setRecommendationTexts(double lAeq) {
        if (measLengthSec > 60) {
            if (lAeq <= 75) {
                setNoLimit(u3Text);
                setNoLimit(b314Text);
                setNoLimit(b1418Text);
            }
            if (lAeq > 75 && lAeq <= 80) {
                setNotRecommend(u3Text);
                setUpTo2(b314Text);
                setNoLimit(b1418Text);
                // lAeqText.setBackgroundColor(Color.YELLOW);
            }
            if (lAeq > 80 && lAeq <= 85) {
                setNotRecommend(u3Text);
                setUpTo45(b314Text);
                setNoLimit(b1418Text);
                // lAeqText.setBackgroundColor(Color.RED);
            }
            if (lAeq > 85 && lAeq <= 90) {
                setNotRecommend(u3Text);
                setNotRecommend(b314Text);
                setUpTo2(b1418Text);
            }
            if (lAeq > 90 && lAeq <= 95) {
                setNotRecommend(u3Text);
                setNotRecommend(b314Text);
                setUpTo45(b1418Text);
            }
            if (lAeq > 95) {
                setNotRecommend(u3Text);
                setNotRecommend(b314Text);
                setNotRecommend(b1418Text);
            }
        }

    }


    private void setNoLimit(BoxedText text) {
        text.textView.setText(R.string.table_no_limit);
        text.setBoxBackground(R.drawable.rounded_bg);
    }

    private void setUpTo45(BoxedText text) {
        text.textView.setText(R.string.table_max_45);
        text.setBoxBackground(R.drawable.rounded_bg_orange);
    }

    private void setUpTo2(BoxedText text) {
        text.textView.setText(R.string.table_max_2);
        text.setBoxBackground(R.drawable.rounded_bg_orange);
    }

    private void setNotRecommend(BoxedText text) {
        text.textView.setText(R.string.table_notrecomm);
        text.setBoxBackground(R.drawable.rounded_bg_red);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                if (!RecorderService.isAlive) {
                    Intent intent = new Intent(this, SettingsActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, R.string.settings_info, Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION.DBA_DBC_BROADCAST_ACTION);
        filter.addAction(Constants.ACTION.LAEQ_BROADCAST_ACTION);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(Constants.ACTION.RECORDERSTOPPED_ACTION);
        registerReceiver(broadcastReceiver, filter);

        if (prefs.getBoolean("u3", true)) {
            u3Text.box.setVisibility(View.VISIBLE);
        } else {
            u3Text.box.setVisibility(View.GONE);
        }
        if (prefs.getBoolean("b314", true)) {
            b314Text.box.setVisibility(View.VISIBLE);
        } else {
            b314Text.box.setVisibility(View.GONE);
        }
        if (prefs.getBoolean("b1418", true)) {
            b1418Text.box.setVisibility(View.VISIBLE);
        } else {
            b1418Text.box.setVisibility(View.GONE);
        }
        String calibPref = prefs.getString(Constants.CALIBTYPE, CalibrationType.NOT_CALIBRATED.toString());
        Constants.calibrationType = CalibrationType.valueOf(calibPref);
        switch (Constants.calibrationType) {
            case NOT_CALIBRATED:
                calibText.setText(R.string.not_calib);
                break;
            case MODELLY_CALIBRATED:
                calibText.setText(R.string.model_calib);
                break;
            case UNIQUELY_CALIBRATED:
                calibText.setText(R.string.uniq_calib);
                break;
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    private void showRationaleDialog(@StringRes int messageResId, final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        request.proceed();
                    }
                })
                .setNegativeButton("DENY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        request.cancel();
                    }
                })
                .setCancelable(false)
                .setMessage(messageResId)
                .show();
    }


    public void resetUI() {
        u3Text.textView.setText("");
        u3Text.setBoxBackground(R.drawable.rounded_bg);
        b314Text.textView.setText("");
        b1418Text.textView.setText("");
        //lAeqText.setBackgroundColor(Color.TRANSPARENT);
        lAeqText.setText("");
    }



    @NeedsPermission({Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void startClick() {
        if (!RecorderService.isAlive) {
            Intent startIntent = new Intent(MainActivity.this, RecorderService.class);
            startIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
            startService(startIntent);
            overloadSumCount = 0;
            makeStopButton();
            resetUI();
        } else {
            Toast.makeText(this, R.string.rec_started, Toast.LENGTH_LONG).show();
        }
    }

    private void stopClick() {
        if (RecorderService.isAlive) {
            Intent stopIntent = new Intent(MainActivity.this, RecorderService.class);
            stopIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
            startService(stopIntent);
        } else {
            Toast.makeText(this, R.string.rec_stopped, Toast.LENGTH_LONG).show();
        }
    }

    private void makeStopButton() {
        startStopText.setText("STOP");
        startStopButton.setOnClickListener(View -> stopClick());
    }

    private void makeStartButton() {
        startStopText.setText("START");
        startStopButton.setOnClickListener(View -> MainActivityPermissionsDispatcher.startClickWithPermissionCheck(this));
    }

    private void toastError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void toastError(@StringRes int message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void dialogError(String message) {
        new android.app.AlertDialog.Builder(MainActivity.this)
                .setCancelable(true)
                .setMessage(message)
                .setTitle(R.string.warning)
                .show();
    }

    private void dialogError(@StringRes int message) {
        new android.app.AlertDialog.Builder(MainActivity.this)
                .setCancelable(true)
                .setMessage(message)
                .setTitle(R.string.warning)
                .show();
    }
}
