package hu.bme.hit.last.ovdafuled;

/**
 * Created by Aron Fabian on 2018. 04. 06..
 */
public enum CalibrationType {
    NOT_CALIBRATED,
    MODELLY_CALIBRATED,
    UNIQUELY_CALIBRATED
}
