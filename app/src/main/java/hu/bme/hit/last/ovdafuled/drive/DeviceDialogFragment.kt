package hu.bme.hit.last.ovdafuled.drive

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import hu.bme.hit.last.ovdafuled.R

class DeviceDialogFragment(
    private val list: List<MyDrive.FDescription>,
    private val listViewCallback: (Int) -> Unit,
    private val nameCallback: (String) -> Unit,
    
) : DialogFragment() {

    class InputDialogFragment(private val callback: (String) -> Unit) : DialogFragment() {
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.input_layout, container, false)
            val okButton = view.findViewById<Button>(R.id.add_device_button_ok)
            val cancelButton = view.findViewById<Button>(R.id.add_device_button_cancel)
            val inputView = view.findViewById<EditText>(R.id.add_device_input)
            isCancelable = false

            cancelButton.setOnClickListener { dismiss() }
            okButton.setOnClickListener {
                val input: String = inputView.text.toString()
                dismiss()
                callback(input)
            }
            return view
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.device_fragment, container, false)
        val plus = view.findViewById<ImageButton>(R.id.plus_button);
        plus.setOnClickListener {
            val inputFragment = InputDialogFragment { result ->
                dismiss()
                nameCallback(result)
            }
            inputFragment.show(parentFragmentManager, "InputFragment")
        }
        val listView = view.findViewById<ListView>(R.id.device_list)
        listView.adapter = MyListAdapter(list)
        listView.setOnItemClickListener { _, _, position, _ ->
            dismiss()
            listViewCallback(position)
        }
        return view
    }
}